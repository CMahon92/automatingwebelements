package automating_webElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Looping_throughDropdown {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		//Looping through dropdown
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
				
				//Thread.sleep(2000);
				
				//clicks the dropdown
				driver.findElement(By.id("divpaxinfo")).click();
				
				Thread.sleep(2000);
				
				//Instead of writing the code 4 times in order to add 4 adults, I created a for loop
				for (int i=1;i<4;i++) {
					
				driver.findElement(By.id("hrefIncAdt")).click();
				
				}
				
				System.out.println("There will be " + driver.findElement(By.id("divpaxinfo")).getText()+"s attending this trip");
	
	}

}
