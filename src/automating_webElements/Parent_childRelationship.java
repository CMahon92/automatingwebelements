package automating_webElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Parent_childRelationship {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Opens Website
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		
		//Clicks from destination dropdown
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		
		//Clicks from destination value
		driver.findElement(By.xpath("//div [contains(@id,'originStation1')] //a[@value='DAC']")).click();
		
		Thread.sleep(2000);
		//Clicks to destination value
		driver.findElement(By.xpath("//div [contains(@id,'destinationStation1')] //a[@value='IXZ']")).click();
		
	
	}

}
