package automating_webElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Handling_Checkboxes {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Opens Website
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
	
		//Family and Friends Checkbox
		driver.findElement(By.xpath("//input[contains(@id,'friendsandfamily')]")).click();
		System.out.println(driver.findElement(By.cssSelector("input[name*='friendsandfamily']")).isSelected());
		Thread.sleep(2000);
		
		//Senior Citizen Checkbox
		System.out.println(driver.findElement(By.cssSelector("input[name*='SeniorCitizen']")).isSelected());
		Assert.assertFalse(driver.findElement(By.cssSelector("input[name*='SeniorCitizen']")).isSelected());
		
		//Indian Arm Forces Checkbox
		driver.findElement(By.xpath("//input[contains(@id,'IndArm')]")).click();
		System.out.println(driver.findElement(By.cssSelector("input[name*='IndArm']")).isSelected());
		Thread.sleep(2000);
		
		//Student Checkbox
		System.out.println(driver.findElement(By.cssSelector("input[name*='Student']")).isSelected());
		
		//Unaccompanied Minor Checkbox
		driver.findElement(By.xpath("//input[contains(@id,'Unmr')]")).click();
		Assert.assertTrue(driver.findElement(By.cssSelector("input[name*='Unmr']")).isSelected());
		System.out.println(driver.findElement(By.cssSelector("input[name*='Unmr']")).isSelected());
		Thread.sleep(2000);
		
		//total checkboxes on webpage
		System.out.println(driver.findElements(By.cssSelector("input[type='checkbox']")).size());
		
		driver.findElement(By.id("divpaxinfo")).click();
		
		Thread.sleep(2000);
		for (int i=1;i<4;i++) {
			
		driver.findElement(By.id("hrefIncChd")).click();
		
		}
		
		//Purposely failed assertEquals test for error message 
		//"expected [6 Child] but found [4 Child]" to print in console
		Assert.assertEquals(driver.findElement(By.id("divpaxinfo")).getText(), "6 Child");
		System.out.println(driver.findElement(By.id("divpaxinfo")).getText());
		
		Thread.sleep(2000);
		driver.quit();
		
		
		
	}

}
