package automating_webElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Dynamic_Dropdown {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		//Handling Dynamic Dropdown
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Opens Website
		driver.get("https://rahulshettyacademy.com/dropdownsPractise/");
		
		//Clicks from destination dropdown
		driver.findElement(By.id("ctl00_mainContent_ddl_originStation1_CTXT")).click();
		
		//Clicks from destination value
		driver.findElement(By.xpath("//a[@value='JRG']")).click();
		
		Thread.sleep(2000);
		//Clicks to destination value 
		//Since the value 'BKK' is also located in the from destination, we need to
		//have the broswer select the second index
		driver.findElement(By.xpath("(//a[@value='BKK'])[2]")).click();
	
	}

}
