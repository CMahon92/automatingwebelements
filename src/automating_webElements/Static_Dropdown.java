package automating_webElements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Static_Dropdown {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

	
		System.setProperty("webdriver.chrome.driver", "//Users//carleenamahon//Documents//chromedriver");
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		//Working with a Static Dropdown
		//Opens website
		driver.get("https://www.globalsqa.com/demo-site/select-dropdown-menu/");
		
		//Provides where the select dropdown is located in HTML
		WebElement staticDropdown = driver.findElement(By.xpath("//div[contains(@class,'resp-tabs')]/div/p/select"));
		
		//Select provides the implementation of the HTML SELECT tag
		//Pass the webelement (staticDropdown) as the argument
		Select countries = new Select(staticDropdown);
		
		countries.selectByValue("AUS");
		System.out.println("I have always wanted to visit " + countries.getFirstSelectedOption().getText());
		
		Thread.sleep(2000);
		
		countries.selectByIndex(21);
		System.out.println(countries.getFirstSelectedOption().getText() + " is best known for their waffles, fries, chocolate and beer.");
	
		Thread.sleep(2000);
		
		countries.selectByVisibleText("Indonesia");
		System.out.println("I hope to go to " + countries.getFirstSelectedOption().getText() +" for my birthday!");
		
		Thread.sleep(2000);
		driver.quit();
		
		
	}

}
